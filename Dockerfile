#Set up base image
FROM node:latest AS base

WORKDIR /work

COPY package.json .

#
# ---- Dependencies ----
FROM base AS dependencies
RUN npm set progress=false
RUN npm install --only=production
#copy prod modules to its own folder
RUN cp -R node_modules prod_node_modules
# install all the modules including dev
RUN npm install

# ---- TEST ----
FROM dependencies AS test
COPY . .
RUN npm run build


# ---- Release ----
FROM base AS release
COPY --from=dependencies /work/prod_node_modules ./node_modules
COPY . .
RUN npm run build
EXPOSE 3000
CMD npm run start

