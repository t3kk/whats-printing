import axios from 'axios'
import Head from 'next/head'
import Image from 'next/image'
import { useEffect, useState, useRef } from 'react'

export default function Home() {

  const [printerState, setPrinterState] = useState()

  //TODO: remove initilal 3s delay
  useInterval(async ()=>{
    let printerState = (await axios.get('/api/hello'))?.data;
    setPrinterState(printerState);
    console.log("updated job")
  }, 3000)

  const unknown= "Unknown";

  let hours, minutes, seconds;

  if (printerState?.progress?.printTimeLeft) {
    let timeRemaining = printerState?.progress?.printTimeLeft;
    hours = Math.floor(timeRemaining / 60 / 60);
    minutes = Math.floor(timeRemaining / 60) - (hours * 60);
    seconds = timeRemaining % 60;
  }

  return (
    <>
      <Head>
        <title>What's Printing?</title>
        <meta name="description" content="What's printing on my printer?" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      
      <img src="/webcam/?action=stream/"/>
      <div>
        <p>State: {printerState?.state || unknown }</p>
        <p>Job Name: {printerState?.job?.file?.name || unknown} </p>
        <p>Progress: {printerState?.progress?.completion ? parseFloat(printerState.progress.completion).toFixed(2)+'%' : unknown} </p>
        <p>Time Remaining: {printerState?.progress?.printTimeLeft ? `${hours} hours, ${minutes} minutes and ${seconds} seconds` : unknown}</p>
      </div>
      
    </>
  )
}

export function useInterval(callback, delay) {
  const savedCallback = useRef();

  useEffect(() => {
    savedCallback.current=callback;
  }, [callback])

  useEffect(() => {
    function tick(){
      savedCallback.current()
    }
    if (delay != null) {
      const id = setInterval(tick, delay);
      return () => {
        clearInterval(id)
      }
    }
  }, [callback, delay])
}
