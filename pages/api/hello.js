// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import axios from 'axios'


export default async (req, res) => {
  let jobReq = await axios.get('http://octopi.thewired/api/job', {headers: {'X-Api-Key': process.env.API_KEY}})
  res.status(200).json(jobReq.data);
}
